﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TreeCardListItemDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	private const float TransitionTime = 0.5f;

	private Card Card;
	public TextMeshProUGUI Text;
	public Image Image;
	public Canvas OnHover;
	public CardPreviewOnHoverDisplay HoverDisplay;

	public void Setup(Card card)
	{
		Card = card;

		Text.text = card.CardName;
		HoverDisplay.CardSetup(card);
	}

	public Card GetCard()
	{
		return Card;
	}

	public void OnColorUpdated(Color color)
	{
		Image.color = color;
	}

	public void DeHighlight()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", Image.color, "to", Color.gray, "time", TransitionTime, "onupdate", "OnColorUpdated"));
	}

	public void Highlight()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", Image.color, "to", Color.yellow, "time", TransitionTime, "onupdate", "OnColorUpdated"));
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		OnHover.enabled = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		OnHover.enabled = false;
	}
}
