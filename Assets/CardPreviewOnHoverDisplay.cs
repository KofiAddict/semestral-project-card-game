﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardPreviewOnHoverDisplay : MonoBehaviour
{
	[HideInInspector]
	public Card Card;

	public TextMeshProUGUI DescriptionText;
	public TextMeshProUGUI CostValueText;
	public TextMeshProUGUI NameText;

	public Image CostIconImage;
	public Image CardImage;

	public Canvas CardBody;

	public void CardSetup(Card thisCard)
	{
		Card = thisCard;
		NameText.text = Card.CardName;
		DescriptionText.text = string.Format(Card.Description, Card.EffectValue);
		CostValueText.text = Card.Cost.ToString();
		CostIconImage.color = Card.CostType == CostType.Mana ? Color.cyan : Color.green;
		CardImage.sprite = thisCard.Art;
	}
}
