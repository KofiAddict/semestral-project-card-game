﻿using UnityEngine;
// ReSharper disable PossibleLossOfFraction

public class MoveToMouseScript : MonoBehaviour
{
    public Transform BackgroundTransform;
	
	// Update is called once per frame
	void Update ()
	{
	    Vector3 mousePosition = Input.mousePosition;
	    mousePosition.x -= Screen.width / 2;
        mousePosition.x *= -0.02f;
	    mousePosition.y -= Screen.height / 2;
	    mousePosition.y *= -0.005f;
		mousePosition.x += Screen.width / 2;
		mousePosition.y += Screen.height / 2;
		mousePosition.z = BackgroundTransform.transform.position.z;
        BackgroundTransform.position = mousePosition;
	}
}
