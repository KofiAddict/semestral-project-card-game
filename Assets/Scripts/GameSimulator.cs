﻿using System;
using System.Collections.Generic;
using System.Linq;

class GameSimulator
{
	private readonly Queue<GameState> upcomingStates = new Queue<GameState>();
	private readonly List<GameState> winningStates = new List<GameState>();
	private readonly List<GameState> losingStates = new List<GameState>();
	private int maxTurnDepth;
	
	public SimulationResult Simulate(EncounterData data, Player player, int turnDepth)
	{
		maxTurnDepth = turnDepth;
		var cards = data.Deck;
		var enemies = data.Enemies.Select(x => x.Clone()).ToList();

		foreach (var enemy in enemies)
		{
			if (enemy.GetType() == typeof(EliteEnemy))
			{
				enemy.ThreatValue *= 5;
			}
		}

		enemies = enemies.OrderByDescending(x => x.ThreatValue).ToList();

		upcomingStates.Enqueue(new GameState
		{
			Enemies = enemies,
			Deck = cards.Skip(5).ToList(),
			DiscardPile = new List<Card>(),
			Energy = player.MaxEnergy,
			Hand = cards.Take(5).ToList(),
			Health = player.MaxHealth,
			Mana = player.MaxMana,
			TurnNum = 1,
			Parent = null
		});

		while (upcomingStates.Count > 0)
		{
			ProcessNextState();
		}

		return new SimulationResult
		{
			EarliestWin = winningStates.Min(x => x.TurnNum),
			WonStates = winningStates,
			LostStates = losingStates
		};
	}

	private void ProcessNextState()
	{
		var currentState = upcomingStates.Dequeue();

		if (currentState.TurnNum > maxTurnDepth)
		{
			return;
		}

		var aoeCards = currentState.Hand.Where(x => x.GetType() == typeof(SpellCard)).ToList();
		var aoeHand = aoeCards.Select(x => (SpellCard) x).ToList();
		var aoeHandCombos = GetAllCombos(aoeHand).Where(x =>
			x.Where(y => y.CostType == CostType.Energy).Sum(y => y.Cost) < currentState.Energy ||
			x.Where(y => y.CostType == CostType.Mana).Sum(y => y.Cost) < currentState.Mana).ToList();

		// for each different combination of aoe cards that can be played
		foreach (var handCombo in aoeHandCombos)
		{
			var nextState = currentState.Clone();

			foreach (var spellCard in handCombo)
			{
				if (spellCard.CostType == CostType.Energy)
				{
					nextState.Energy -= spellCard.Cost;
				}
				else
				{
					nextState.Mana -= spellCard.Cost;
				}

				switch (spellCard.BaseEffectType)
				{
					case EffectType.Heal:
						nextState.Health = nextState.Health + spellCard.EffectValue > nextState.MaxHealth
							? nextState.MaxHealth
							: nextState.Health + spellCard.EffectValue;
						break;
					case EffectType.Damage:
						foreach (var enemy in nextState.Enemies)
						{
							enemy.Health -= enemy.Health - spellCard.EffectValue < 0
								? enemy.Health
								: spellCard.EffectValue;
						}

						break;
					case EffectType.ReduceDamage:
						foreach (var enemy in nextState.Enemies)
						{
							enemy.Attack -= spellCard.EffectValue < 0 ? enemy.Attack : spellCard.EffectValue;
						}

						break;
					case EffectType.ManaFill:
						nextState.Mana = nextState.Mana + spellCard.EffectValue > nextState.MaxMana
							? nextState.MaxMana
							: nextState.Mana + spellCard.EffectValue;
						break;
					case EffectType.EnergyFill:
						nextState.Energy = nextState.Energy + spellCard.EffectValue > nextState.MaxEnergy
							? nextState.MaxEnergy
							: nextState.Energy + spellCard.EffectValue;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			var singleTargetHand = currentState.Hand.Except(aoeCards).ToList();
			var singleTargetHandCombos = GetAllCombos(singleTargetHand).Where(x =>
				x.Where(y => y.CostType == CostType.Energy).Sum(y => y.Cost) < nextState.Energy ||
				x.Where(y => y.CostType == CostType.Mana).Sum(y => y.Cost) < nextState.Mana).ToList();

			foreach (var singleTargetHandCombo in singleTargetHandCombos)
			{
				var remainingEnergy = nextState.Energy -
				                      singleTargetHandCombo.Where(y => y.CostType == CostType.Energy)
					                      .Sum(y => y.Cost);
				var remainingMana = nextState.Mana -
				                    singleTargetHandCombo.Where(y => y.CostType == CostType.Mana)
					                    .Sum(y => y.Cost);

				if (singleTargetHand.Except(singleTargetHandCombo).Any(x =>
					x.CostType == CostType.Energy ? x.Cost <= remainingEnergy : x.Cost <= remainingMana))
				{
					continue;
				}

				var nextNextState = nextState.Clone();

				nextNextState.Mana = remainingMana;
				nextNextState.Energy = remainingEnergy;

				foreach (var card in singleTargetHandCombo)
				{
					var nextTarget = nextNextState.Enemies.SkipWhile(x => x.Health == 0).FirstOrDefault();

					if (nextTarget == null)
					{
						nextNextState.Won = true;
						winningStates.Add(nextNextState);
						break;
					}

					switch (card.BaseEffectType)
					{
						case EffectType.Heal:
							break;
						case EffectType.Damage:
							nextTarget.Health -= nextTarget.Health - card.EffectValue < 0
								? nextTarget.Health
								: card.EffectValue;
							break;
						case EffectType.ReduceDamage:
							nextTarget.Attack -= card.EffectValue < 0 ? nextTarget.Attack : card.EffectValue;
							break;
						case EffectType.ManaFill:
							break;
						case EffectType.EnergyFill:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}

				if (nextNextState.Won)
				{
					continue;
				}

				foreach (var enemy in nextNextState.Enemies.Where(x => x.Health > 0))
				{
					nextNextState.Health -= enemy.Attack;
				}

				if (nextNextState.Health < 1)
				{
					nextNextState.Lost = true;
					losingStates.Add(nextNextState);
					break;
				}

				++nextNextState.TurnNum;

				upcomingStates.Enqueue(nextNextState);
			}
		}
	}

	public class TurnSimulationResult
	{
		public GameState BestResult;
		public GameState MedianResult;
	}

	public class SimulationResult
	{
		public int EarliestWin { get; internal set; }
		public List<GameState> WonStates;
		public List<GameState> LostStates;
	}

	/// <summary>
	/// Gives all combinations of given list, yes i blatantly copied it :)
	/// </summary>
	/// <typeparam name="T">Entity to make combinations of</typeparam>
	/// <param name="list">Base list to make combinations from</param>
	/// <returns>List of all combinations of given list</returns>
	private static List<List<T>> GetAllCombos<T>(List<T> list)
	{
		List<List<T>> result = new List<List<T>> {new List<T>()};
		// head
		result.Last().Add(list[0]);
		if (list.Count == 1)
			return result;
		// tail
		List<List<T>> tailCombos = GetAllCombos(list.Skip(1).ToList());
		tailCombos.ForEach(combo =>
		{
			result.Add(new List<T>(combo));
			combo.Add(list[0]);
			result.Add(new List<T>(combo));
		});
		return result;
	}

	public class GameState
	{
		public List<Card> Hand;
		public List<Card> Deck;
		public List<Card> DiscardPile;

		public int MaxHealth;
		public int MaxMana;
		public int MaxEnergy;
		public int Health;
		public int Mana;
		public int Energy;

		public int TurnNum;

		public List<Enemy> Enemies;

		public bool Won;
		public bool Lost;

		public GameState Parent;

		public GameState Clone()
		{
			var newOne = new GameState
			{
				Hand = Hand.ToList(),
				Enemies = Enemies.Select(x => x.Clone()).ToList(),
				Energy = Energy,
				Mana = Mana,
				DiscardPile = DiscardPile.ToList(),
				Parent = this,
				Lost = Lost,
				Deck = Deck.ToList(),
				TurnNum = TurnNum,
				Health = Health,
				Won = Won,
				MaxEnergy = MaxEnergy,
				MaxMana = MaxMana,
				MaxHealth = MaxHealth
			};

			return newOne;
		}
	}
}