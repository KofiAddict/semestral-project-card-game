﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NonTargetablePlayable : MonoBehaviour, IDragHandler, IBeginDragHandler,IEndDragHandler, ICardPlayable
{
    public float ToScreenRatio;
    public Transform parentToReturnTo = null;
    public Transform placeholderParent = null;
    public GameObject ImageGameObject;
    public GameObject OtherImageGameObject;
    public GameObject placeholder = null;
	public bool Disabled;
	public bool BeingPlayed;

	public void OnBeginDrag(PointerEventData eventData)
	{
		GetComponent<SpellCardDisplay>().GameManager.OnDragNonTargetedSpellCard();

		if (Disabled)
		{
			return;
		}

		ToScreenRatio = 1920f / Screen.width;
        placeholder = new GameObject();
        placeholder.transform.SetParent(transform.parent);
        LayoutElement le = placeholder.AddComponent<LayoutElement>();
        le.preferredWidth = GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = GetComponent<LayoutElement>().preferredHeight;
        le.flexibleWidth = 0;
        le.flexibleHeight = 0;

        ImageGameObject.GetComponent<Image>().raycastTarget = false;
        OtherImageGameObject.GetComponent<Image>().raycastTarget = false;
        placeholder.transform.SetSiblingIndex(transform.GetSiblingIndex());

        parentToReturnTo = transform.parent;
        placeholderParent = parentToReturnTo;
        transform.SetParent(transform.parent.parent);
    }

    public void OnDrag(PointerEventData eventData)
	{
		if (Disabled)
		{
			return;
		}

		var mouseVector = new Vector3(eventData.position.x * ToScreenRatio,
            eventData.position.y * ToScreenRatio);
        transform.position = mouseVector;

        if (placeholder.transform.parent != placeholderParent)
            placeholder.transform.SetParent(placeholderParent);

        int newSiblingIndex = placeholderParent.childCount;

        for (int i = 0; i < placeholderParent.childCount; i++)
        {
            if (transform.position.x < placeholderParent.GetChild(i).position.x)
            {

                newSiblingIndex = i;

                if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
                    newSiblingIndex--;

                break;
            }
        }

        placeholder.transform.SetSiblingIndex(newSiblingIndex);

    }

    public void OnEndDrag(PointerEventData eventData)
	{
		GetComponent<SpellCardDisplay>().GameManager.OnDropNonTargetedSpellCard();

		if (Disabled)
		{
			return;
		}
		GetComponent<SpellCardDisplay>().GameManager.HoveredObject = null;

		if (!BeingPlayed)
		{
			transform.SetParent(parentToReturnTo); 
		}

        transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        ImageGameObject.GetComponent<Image>().raycastTarget = true;
        OtherImageGameObject.GetComponent<Image>().raycastTarget = true;

        Destroy(placeholder);
    }

	public void Disable()
	{
		Disabled = true;
	}

	public void Enable()
	{
		Disabled = false;
	}
}
