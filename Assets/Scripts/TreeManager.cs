﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TreeManager : MonoBehaviour
{
	private const int TopListItemY = 490;
	private const int MaximumHeightForOnHover = 350;
	private const int DeckListXCoordinate = -785;

	public GameObject Line;
	public GameObject NodePrefab;
	public GameObject TreeListItemPrefab;
	public Text NumberOfPointsLeftText;
	public NextEncounterButtonHandler NextEncounterButtonHandler;
	private readonly List<GameObject> Nodes = new List<GameObject>();
	private readonly Dictionary<Card, GameObject> ListItems = new Dictionary<Card, GameObject>();
	private IEncounterCreator creator = new EncounterCreatorSequential();

	public Canvas BackgroundCanvas;

	void Start()
	{
		InstantiateNewTree();
		InstantiateDeckList();
		UpdateValues();
	}

	void InstantiateDeckList()
	{
		var deck = CrossSceneData.NextEncounterData.Deck.OrderBy(x => x.Cost).ToList(); // last encounter data


		for (int i = 0; i < deck.Count; i++)
		{
			var vector = new Vector3(DeckListXCoordinate, TopListItemY - i * 60);
			var go = Instantiate(TreeListItemPrefab, vector, Quaternion.identity);
			go.GetComponent<TreeCardListItemDisplay>().Setup(deck[i]);

			if (TopListItemY - i * 60 > MaximumHeightForOnHover)
			{
				var display = go.GetComponent<TreeCardListItemDisplay>();

				display.OnHover.transform.position += new Vector3(0, MaximumHeightForOnHover - (TopListItemY - i * 60));
			}

			if (TopListItemY - i * 60 < -MaximumHeightForOnHover)
			{
				var display = go.GetComponent<TreeCardListItemDisplay>();

				display.OnHover.transform.position += new Vector3(0, -MaximumHeightForOnHover + (TopListItemY - i * 60));
			}

			ListItems.Add(deck[i], go);
		}
	}

	void InstantiateNewTree()
	{
		var root = CrossSceneData.Tree.Root;
		var previousLayer = new List<INode>();
		var currentLayer = new List<INode> {root};
		var treeViewSpace = 1500f;

		var currentLayerIndex = 1;
		var verticalSpacing = 100;
		var spacing = treeViewSpace / (currentLayer.Count + 1);

		while (currentLayer.Count > 0)
		{
			var previousSpacing = spacing;
			spacing = treeViewSpace / (currentLayer.Count + 1);
			var index = 1;

			foreach (var node in currentLayer)
			{
				// new node in current layer
				var instantiate = Instantiate(NodePrefab,
					new Vector3(index * spacing - treeViewSpace / 2, verticalSpacing * currentLayerIndex - 500),
					Quaternion.identity);
				instantiate.GetComponent<NodeDisplay>().NodeSetup(node);
				instantiate.GetComponent<NodeDisplay>().TreeManager = this;
				Nodes.Add(instantiate);

				// create line to parent
				if (previousLayer.Any())
				{
					var parentVector = new Vector3((previousLayer.IndexOf(node.Parent) + 1) * previousSpacing - treeViewSpace/2,
						verticalSpacing * (currentLayerIndex - 1) - 475);
					var currentVector = instantiate.transform.position - new Vector3(0, 25f);

					var deltaVector = currentVector - parentVector;

					var angle = (float) Math.Atan(Math.Abs(deltaVector.x) / deltaVector.y) * 57.296f;

					var line = Instantiate(Line, parentVector,
						Quaternion.Euler(0, 0, deltaVector.x < 0 ? angle : -angle));

					line.transform.localScale = new Vector3(1, deltaVector.magnitude / 100f, 1);
				}

				++index;
			}

			var newList = new List<INode>();

			foreach (var node in currentLayer)
			{
				if (node.ConnectedTo != null)
				{
					newList.AddRange(node.ConnectedTo);
				}
			}

			previousLayer = currentLayer;
			currentLayer = newList;

			++currentLayerIndex;
		}
	}

	public bool TrySpend()
	{
		if (CrossSceneData.Player.PointsToSpend <= 0)
		{
			return false;
		}

		--CrossSceneData.Player.PointsToSpend;

		if (CrossSceneData.Player.PointsToSpend == 0)
		{
			NextEncounterButtonHandler.Highlight();
		}

		return true;
	}

	public void UnSpend()
	{
		++CrossSceneData.Player.PointsToSpend;
		NextEncounterButtonHandler.DeHighlight();
	}

	private void UpdateValues()
	{
		NumberOfPointsLeftText.text = CrossSceneData.Player.PointsToSpend.ToString();
		if (CrossSceneData.Player.PointsToSpend > 0)
		{
			NextEncounterButtonHandler.DeHighlight();
		}
		else
		{
			NextEncounterButtonHandler.Highlight();
		}

		foreach (var listItemsValue in ListItems.Values)
		{
			listItemsValue.GetComponent<TreeCardListItemDisplay>().DeHighlight();
		}

		if (CrossSceneData.Tree.GetSelectedNodes().Any())
		{
			foreach (var card in CrossSceneData.NextEncounterData.Deck.Where(card =>
				CrossSceneData.Tree.GetSelectedNodes()
					.Select(x => x.Targets.Select(NodeEffects.GetPredicate)
					.Aggregate((y, z) => cardToCheck => y(cardToCheck) && z(cardToCheck))) 
					.Aggregate((x, y) => cardToCheck => x(cardToCheck) || y(cardToCheck))(card))) // is this haskell?
			{
				ListItems[card].GetComponent<TreeCardListItemDisplay>().Highlight();
			} 
		}
	}

	public bool IsNodePurchasable(INode node)
	{
		return node.Parent.NodeState == NodeState.Bought || node.Parent.NodeState == NodeState.Selected;
	}

	public bool IsNodeDeSelectable(INode node)
	{
		if (node.ConnectedTo == null)
		{
			return true;
		}

		return node.ConnectedTo.All(x => x.NodeState != NodeState.Selected);
	}

	public void OnNextEncounterPress()
	{
		if (CrossSceneData.Player.PointsToSpend > 0)
		{
			return;
		}

		foreach (var node in CrossSceneData.Tree.GetSelectedNodes())
		{
			node.NodeState = NodeState.Bought;
		}

		creator = CrossSceneData.EncounterEnemySets == null
			? (IEncounterCreator)new EncounterCreatorNaive()
			: new EncounterCreatorSequential(); 

		CrossSceneData.NextEncounterData = creator.CreateEncounterData(CrossSceneData.Tree,
			CrossSceneData.NextEncounterData.Deck, CrossSceneData.EncounterNumber++);

		SceneManager.LoadScene("Encounter");
	}

	public void OnAnyChange()
	{
		UpdateValues();
	}
}
