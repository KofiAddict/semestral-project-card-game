﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

[CreateAssetMenu(fileName = "New Creature", menuName = "Enemy/New")]
public class Enemy : GameElement
{
    [HideInInspector] public int Attack;
    [HideInInspector] public int Health;
    public int BaseAttack;
    public int BaseHealth;
    public int ThreatValue;

	public virtual EnemyRarity Rarity
	{
		get { return EnemyRarity.Normal; }
		set { }
	}

	public virtual IList<EndTurnEffect> EndTurnEffects
	{
		get { return new List<EndTurnEffect>(); }
		set { }
	}

	public virtual Action<GameManager> EndTurnEffectAction
	{
		get { return x => { }; }
		set { }
	}

	public void Reset()
    {
        Attack = BaseAttack;
        Health = BaseHealth;
    }

	public EnemyEntity ToEntity()
	{
		return new EnemyEntity()
		{
			SpriteName = Art.name,
			Attack = BaseAttack,
			Defense = BaseHealth,
			Name = CardName
		};
	}

    public virtual Enemy Clone()
    {
        var newEnemy = CreateInstance<Enemy>();

        newEnemy.Health = Health;
        newEnemy.Attack = Attack;
        newEnemy.ThreatValue = ThreatValue;
        newEnemy.BaseAttack = BaseAttack;
        newEnemy.BaseHealth = BaseHealth;

        newEnemy.name = name;
        newEnemy.CardName = CardName;
        newEnemy.Description = Description;
        newEnemy.Art = Art;


        return newEnemy;
    }

	public EliteEnemy GetEliteVersion()
	{
		var newEnemy = CreateInstance<EliteEnemy>();

		newEnemy.Health = Health;
		newEnemy.Attack = Attack;
		newEnemy.ThreatValue = ThreatValue;
		newEnemy.BaseAttack = BaseAttack;
		newEnemy.BaseHealth = BaseHealth;

		newEnemy.name = name;
		newEnemy.CardName = CardName;
		newEnemy.Description = Description;
		newEnemy.Art = Art;

		Random r = new Random();
		newEnemy.Rarity = r.Next(4) == 3 ? EnemyRarity.Rare : EnemyRarity.Magic;


		List<EndTurnEffect> newEnemyEndTurnEffects;
		if (newEnemy.Rarity == EnemyRarity.Magic)
		{
			newEnemyEndTurnEffects = new List<EndTurnEffect> {(EndTurnEffect) r.Next(5)};
		}
		else
		{
			newEnemyEndTurnEffects = new List<EndTurnEffect>();
			newEnemyEndTurnEffects.Add((EndTurnEffect)r.Next(5));
			newEnemyEndTurnEffects.Add((EndTurnEffect)r.Next(5));
		}
		newEnemy.EndTurnEffects = newEnemyEndTurnEffects;
		var newEnemyEndTurnEffectsValues = newEnemy.Rarity == EnemyRarity.Magic
			? new List<int> { r.Next(1,4) }
			: new List<int> { r.Next(1,4), r.Next(1,4) };
		newEnemy.EndTurnEffectsValues = newEnemyEndTurnEffectsValues;
		
		for (int i = 0; i < newEnemyEndTurnEffects.Count; i++)
		{
			newEnemy.EndTurnEffectAction += global::EndTurnEffects.GetEndTurnEffect(newEnemy, newEnemyEndTurnEffects[i],
				newEnemyEndTurnEffectsValues[i]);
		}
		

		return newEnemy;
	}
}
