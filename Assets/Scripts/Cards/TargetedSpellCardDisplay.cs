﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TargetedSpellCardDisplay : MonoBehaviour, ICardDisplay, IPointerEnterHandler, IPointerExitHandler
{
    [HideInInspector]
    public TargetedSpellCard Card;
	public GameManager GameManager { get; set; }
	public InfoPanelDisplay InfoPanelDisplay;

	public TextMeshProUGUI DescriptionText;
    public TextMeshProUGUI CostValueText;
    public TextMeshProUGUI NameText;

    public Image CostIconImage;
    public Image CardImage;
	public Image DimOverlayImage;
	public Image CostDimOverlayImage;

    public void CardSetup(TargetedSpellCard thisCard, GameManager gameManager)
    {
        Card = thisCard;
        NameText.text = Card.CardName;
        DescriptionText.text = string.Format(Card.Description,Card.EffectValue);
        CostValueText.text = Card.Cost.ToString();
        CostIconImage.color = Card.CostType == CostType.Mana ? Color.cyan : Color.green;
        CardImage.sprite = thisCard.Art;
	    GameManager = gameManager;
    }

	public void Disable()
	{
		Dim();
		CostValueText.faceColor = new Color32(255, 0, 0, 255);
	}

	public void Enable()
	{
		LightUp();
		CostValueText.faceColor = new Color32(255, 255, 255, 255);
	}

	public Card GetCard()
	{
		return Card;
	}

	public void Dim()
	{
		DimOverlayImage.enabled = true;
		CostDimOverlayImage.enabled = true;
	}

	public void LightUp()
	{
		DimOverlayImage.enabled = false;
		CostDimOverlayImage.enabled = false;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		InfoPanelDisplay.Show(Card);
		if (!eventData.dragging)
		{
			GameManager.HoveredObject = Card; 
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		InfoPanelDisplay.Clear();
		if (!eventData.dragging)
		{
			GameManager.HoveredObject = null; 
		}
	}

	public void DrawAnimation(int position)
	{
		iTween.ScaleTo(gameObject, new Vector3(1.917983f, 1.917983f, 1.917983f), 1f);
		iTween.MoveTo(gameObject, new Vector3(651.5001f + 250 * position, 166.155f, -0.25f), 1f);
	}

	public void DiscardAnimation()
	{
		transform.SetParent(transform.parent.parent);
		iTween.ScaleTo(gameObject, new Vector3(1f, 1f, 1f), 1f);
		iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(1848.04f, 100.975f, -0.25f), "time", 1f, "oncomplete", "DestroyItself"));
	}

	public void DestroyItself()
	{
		Destroy(gameObject);
	}
}
