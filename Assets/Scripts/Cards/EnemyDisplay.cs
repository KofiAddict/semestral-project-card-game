﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnemyDisplay : MonoBehaviour, IDropHandler, ITargetable, IPointerEnterHandler, IPointerExitHandler
{
	[HideInInspector] public Enemy Enemy;

	public TextMeshProUGUI AttackValueText;
	public TextMeshProUGUI HealthValueText;
	public TextMeshProUGUI NameText;

	public Image EnemyImage;
	public GameManager GameManager;
	public Image DeathOverlayImage;
	public InfoPanelDisplay InfoPanelDisplay;

	public bool IsDead;

	private int maxHealth;
	// private int startAttack;

	public void CardSetup(Enemy thisEnemy)
	{
		Enemy = thisEnemy;

		maxHealth = Enemy.Health;
		// startAttack = Enemy.Attack;

		EnemyImage.sprite = Enemy.Art;
		if (thisEnemy.Rarity == EnemyRarity.Magic)
		{
			EnemyImage.color = new Color(0, 0.5f, 1, 1);
		}
		else if (thisEnemy.Rarity == EnemyRarity.Rare)
		{
			EnemyImage.color = Color.yellow;
		}

		UpdateValues();
	}

	public void UpdateValues()
	{
		NameText.text = Enemy.CardName;

		AttackValueText.text = Enemy.Attack.ToString();
		HealthValueText.text = Enemy.Health.ToString();
	}

	public void OnDrop(PointerEventData eventData)
	{
		var spell = eventData.pointerDrag.GetComponent<TargetedSpellCardDisplay>();

		if (spell != null)
		{
			if (IsDead)
			{
				GameManager.ShowInfoMessage(Enemy, "Enemy is dead already");
				return;
			}

			if (spell.Card.CostType == CostType.Energy)
			{
				if (GameManager.PlayerStats[2].GetComponent<SettableStat>().TrySpend(spell.Card.Cost))
				{
					spell.Card.Effect.Invoke(this);
					GameManager.OnCardUse(spell.Card, Enemy);
					GameManager.DiscardCard(eventData.pointerDrag, spell.Card);
				}
				else
				{
					GameManager.ShowInfoMessage(spell.Card, "Not enough energy!");
					GameManager.HoveredObject = null;
				}
			}
			else
			{
				if (GameManager.PlayerStats[1].GetComponent<SettableStat>().TrySpend(spell.Card.Cost))
				{
					spell.Card.Effect.Invoke(this);
					GameManager.OnCardUse(spell.Card, Enemy);
					GameManager.DiscardCard(eventData.pointerDrag, spell.Card);
				}
				else
				{
					GameManager.ShowInfoMessage(spell.Card, "Not enough mana!");
					GameManager.HoveredObject = null;
				}
			}
		}
	}

	public int Heal(int amount)
	{
		if (IsDead)
		{
			return 0;
		}

		Enemy.Health += amount;
		if (Enemy.Health > maxHealth)
		{
			Enemy.Health = maxHealth;
		}

		UpdateValues();

		return Enemy.Health;
	}

	public int RemainingHealth
	{
		get { return Enemy.Health; }
	}

	public int Damage(int damage)
	{
		if (IsDead)
		{
			return 0;
		}

		Enemy.Health -= damage;
		if (Enemy.Health <= 0)
		{
			Enemy.Health = 0;
			Die();
			GameManager.OnDeadEnemy();
		}

		UpdateValues();

		return Enemy.Health;
	}

	public int ReduceDamage(int amount)
	{
		Enemy.Attack -= amount;

		if (Enemy.Attack < 0)
		{
			Enemy.Attack = 0;
		}

		UpdateValues();

		return Enemy.Attack;
	}

	public void Die()
	{
		IsDead = true;

		iTween.ValueTo(gameObject,
			iTween.Hash("time", 0.5f, "from", 0f, "to", 1f, "onupdate", "DeathOverlayAlphaUpdate"));
	}

	public void DeathOverlayAlphaUpdate(float value)
	{
		DeathOverlayImage.color = new Color(1f, 1f, 1f, value);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		InfoPanelDisplay.Show(Enemy);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		InfoPanelDisplay.Clear();
	}
}
