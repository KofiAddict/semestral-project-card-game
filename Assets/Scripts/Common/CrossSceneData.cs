﻿using System.Collections.Generic;

public static class CrossSceneData
{
	public static List<List<Enemy>> EncounterEnemySets;
    public static List<Enemy> PossibleEnemies;
    public static EncounterData NextEncounterData = new EncounterData();
    public static Player Player;
    public static Card[] OriginalCards;
    public static ITree Tree;
    public static int EncounterNumber = 1;
	public static bool ShowTutorial = false;
}
