﻿using System;

public static class Effects
{
    public static Action<ITargetable> DamageAction(int damage)
    {
        return targetable => { targetable.Damage(damage); };
    }

    public static Action<ITargetable> ReduceDamageAction(int damage)
    {
        return targetable => { targetable.ReduceDamage(damage); };
    }

    public static Action<GameManager> HealAction(int amount)
    {
        return manager => { manager.PlayerStats[0].GetComponent<SettableStat>().Fill(amount); };
    }

    public static Action<GameManager> ManaGainAction(int amount)
    {
        return manager => { manager.PlayerStats[1].GetComponent<SettableStat>().Fill(amount); };
    }

    public static Action<GameManager> EnergyGainAction(int amount)
    {
        return manager => { manager.PlayerStats[2].GetComponent<SettableStat>().Fill(amount); };
    }

    public static Action<GameManager> DamageAllAction(int damage)
    {
        return manager =>
        {
            foreach (var enemy in manager.EnemiesObjects)
            {
                enemy.GetComponent<EnemyDisplay>().Damage(damage);
            }
        };
    }

    public static Action<GameManager> ReduceDamageAllAction(int effectValue)
    {
        return manager =>
        {
            foreach (var enemiesObject in manager.EnemiesObjects)
            {
                enemiesObject.GetComponent<EnemyDisplay>().ReduceDamage(effectValue);
            }
        };
    }
}
