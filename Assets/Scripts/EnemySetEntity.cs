﻿using System;

[Serializable]
class EnemySetEntity
{
	public EnemyEntity[] Entities;
}

[Serializable]
public class EnemyEntity
{
	public int Attack;
	public int Defense;
	public string SpriteName;
	public string Name;
}
