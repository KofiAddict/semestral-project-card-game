﻿using System;

[Serializable]
class Encounter
{
	public int[] TreeChoices;
	public EnemyEntity[] Enemies;
	public Turn[] Turns;
	public EncounterOutcome Outcome;
}

public enum EncounterOutcome
{
	Quit, Won, Lost
}