﻿using System;

[Serializable]
class Playthrough
{
	public CardEntity[] Deck;
	public Encounter[] Encounters;
	public NodeEntities Tree;
}
