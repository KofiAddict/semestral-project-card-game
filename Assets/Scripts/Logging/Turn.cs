﻿using System;

[Serializable]
class Turn
{
	public CardEntity[] StartingHand;
	public CardPlay[] CardPlays;
	public int DamageTaken;
}
