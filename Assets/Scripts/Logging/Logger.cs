﻿/*
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

public static class Logger
{
	private const string LogSubfolder = "./Log/";
	private static string _currentFileName;
	public static string Cache = "";
	private static Thread _loggingThread;
	private static Playthrough _playthrough;
	private static Encounter _encounter;
	private static Turn _currentTurn;
	private static List<Encounter> _encounters;
	private static List<Turn> _turns;
	private static List<Card> _currentHand;
	private static List<Card> _played;
	private static List<Enemy> _enemies;
	private static List<Enemy> _targets;

	public static void CreateNewLogFile()
	{
		Directory.CreateDirectory("./Log");
		_currentFileName = "GameLog " + DateTime.Now.ToString("yyyy-MMM-dd hh-mm-ss") + ".log";

		if (_loggingThread != null && _loggingThread.IsAlive)
		{
			_loggingThread.Abort();
		}

		var thread = new Thread(() =>
		{
			while (true)
			{
				if (!string.IsNullOrEmpty(Cache))
				{
					File.AppendAllText(LogSubfolder + _currentFileName, Logger.Cache + "\n");
					Cache = "";
				}

				Thread.Sleep(1000);
			}
		});
		_loggingThread = thread;
		_loggingThread.Start();
	}

	public static void Log(string message)
	{
		if (string.IsNullOrEmpty(_currentFileName))
		{
			CreateNewLogFile();
		}
	}

	public static void StartNewSession(NodeTree nodeTree, IEnumerable<Card> deck)
	{
		_playthrough = new Playthrough()
		{
			Tree = new NodeEntities()
			{
				Entities = nodeTree.GetAllNodes().Select(x => x.ToEntity()).ToArray()
			},
			Deck = deck.Select(card => card.ToEntity()).ToArray()
		};
		_encounters = new List<Encounter>();
	}

	public static void StartNewEncounter(List<Enemy> enemies, IEnumerable<int> selectedNodeIds)
	{
		_encounter = new Encounter()
		{
			Enemies = enemies.Select(x => x.ToEntity()).ToArray(),
			TreeChoices = selectedNodeIds.ToArray()
		};
		_enemies = enemies;
		_turns = new List<Turn>();
	}

	public static void StartNewTurn(Card[] cards)
	{
		_currentHand = cards.ToList();
		_currentTurn = new Turn()
		{
			StartingHand = cards.Select(x => x.ToEntity()).ToArray()
		};
		_played = new List<Card>();
		_targets = new List<Enemy>();
	}

	public static void RegisterCardPlay(TargetedSpellCard card, Enemy target)
	{
		_played.Add(card);
		_targets.Add(target);
	}

	public static void RegisterCardPlay(SpellCard card)
	{
		_played.Add(card);
		_targets.Add(null);
	}

	public static void FinishTurn(int damageTaken)
	{
		List<CardPlay> cardPlays = new List<CardPlay>();
		for (int i = 0; i < _played.Count; i++)
		{
			cardPlays.Add(new CardPlay()
			{
				IndexOfCardPlayed = _currentHand.IndexOf(_played[i]),
				IndexOfTarget = _enemies.IndexOf(_targets[i])
			});
		}

		_currentTurn.CardPlays = cardPlays.ToArray();

		_currentTurn.DamageTaken = damageTaken;

		_turns.Add(_currentTurn);
	}

	public static void FinishEncounter(EncounterOutcome encounterOutcome)
	{
		_encounter.Outcome = encounterOutcome;
		_encounter.Turns = _turns.ToArray();
		_encounters.Add(_encounter);
	}

	public static void FinishPlaythrough()
	{
		_playthrough.Encounters = _encounters.ToArray();
		Directory.CreateDirectory("./Log");
		_currentFileName = "GameLog " + DateTime.Now.ToString("yyyy-MMM-dd hh-mm-ss") + ".log";
		File.AppendAllText(LogSubfolder + _currentFileName, JsonUtility.ToJson(_playthrough));
	}
}
*/
