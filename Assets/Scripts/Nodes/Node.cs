﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New Node", menuName = "Nodes/New")]
public class Node : ScriptableObject, INode, ICloneable
{
	public int Id;
    public Node[] Children;
    public Action<IEnumerable<Card>> EffectAction;
    public INode Parent { get; set; }
    public NodeTarget[] NodeTargets;

	public IEnumerable<INode> ConnectedTo
    {
        get { return Children; }
        set { Children = (Node[])(value.ToArray()); }
    }

    public NodeState NodeState
    {
        get { return nodeState; }
        set { nodeState = value; }
    }

    private NodeState nodeState = NodeState.None;

    public Sprite Sprite
    {
        get { return IconSprite; }
        set { IconSprite = value; }
    }

    public string DescriptionString
    {
        get { return Description; }
        set { Description = value; }
    }

    public int NodePower
    {
        get { return nodePower; }
        set { nodePower = value; }
    }

    public int nodePower;

    public Sprite IconSprite;
    public string Description;

	public void ApplyEffectToCards(IEnumerable<Card> cards)
    {
        EffectAction.Invoke(cards);
    }

	public IEnumerable<NodeTarget> Targets
	{
		get { return NodeTargets; }
		set { NodeTargets = value.ToArray(); }
	}

	public bool IsBought
    {
        get { return NodeState == NodeState.Bought; }
    }

    public object Clone()
    {
        var node = CreateInstance<Node>();
        node.EffectAction = EffectAction;
        node.Children = Children;
        node.Parent = Parent;
        node.NodeTargets = NodeTargets.ToArray();
        node.NodeState = NodeState;
        node.NodePower = NodePower;
        node.IconSprite = IconSprite;
        node.Description = Description;
        return node;
    }

	public static Node FromEntity(NodeEntity entity)
	{
		var node = CreateInstance<Node>();

		node.Description = string.IsNullOrEmpty(entity.Description) ? "Increase effect of all " +
						   (entity.Targets.Contains(NodeTarget.Energy) ? "energy " : string.Empty) + 
		                   (entity.Targets.Contains(NodeTarget.Mana) ? "mana " : string.Empty) + 
		                   (entity.Targets.Contains(NodeTarget.SingleTarget) ? "targeted " : string.Empty) + 
		                   (entity.Targets.Contains(NodeTarget.MultiTarget) ? "area " : string.Empty) + 
		                   (entity.Targets.Contains(NodeTarget.Damage) ? "damaging " : string.Empty) + 
		                   (entity.Targets.Contains(NodeTarget.Heal) ? "healing " : string.Empty) + "cards by " + entity.EffectValue + "." 
			: entity.Description; 
	
		node.Id = entity.Id;
		node.nodePower = entity.EffectValue;
		node.Targets = entity.Targets;

		return node;
	}

	public NodeEntity ToEntity()
	{
		return new NodeEntity
		{
			Id = Id,
			ChildrenIds = Children.Select(x => x.Id).ToArray(),
			EffectValue = NodePower,
			Targets = NodeTargets
		};
	}
}
