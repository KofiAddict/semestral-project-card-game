﻿using System.Collections.Generic;

public class EncounterData
{
    public List<Enemy> Enemies;
    public List<Card> Deck;
}