﻿public class Player
{
    public int BaseMaxHealth = 30;
    public int MaxHealth;
    public int CurrentHealth;

    public int BaseMaxMana = 10;
    public int MaxMana;
    public int CurrentMana;

    public int BaseMaxEnergy = 50;
    public int MaxEnergy;
    public int CurrentEnergy;

    public int PointsToSpend;

    public void Restore()
    {
        CurrentEnergy = MaxEnergy;
        CurrentHealth = MaxHealth;
        CurrentMana = MaxMana;
    }

    public void Reset()
    {
        MaxEnergy = BaseMaxEnergy;
        MaxHealth = BaseMaxHealth;
        MaxMana = BaseMaxMana;
        Restore();
    }
}
