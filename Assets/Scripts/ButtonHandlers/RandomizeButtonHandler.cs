﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RandomizeButtonHandler : MonoBehaviour, IPointerClickHandler
{
    public GameStarter GameStarter;

    public void OnPointerClick(PointerEventData eventData)
    {
        CrossSceneData.Tree = GameStarter.GetRandomTree();
        GameStarter.Randomize();
        GameStarter.StartEncounter();
    }
}
