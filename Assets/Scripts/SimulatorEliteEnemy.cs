﻿
	using System;
	using System.Collections.Generic;

public class SimulatorEliteEnemy : SimulatorEnemy
{
	public override IList<EndTurnEffect> EndTurnEffects { get; set; }

	public override EnemyRarity Rarity { get; set; }

	public List<int> EndTurnEffectsValues { get; set; }

	public override SimulatorEnemy Clone()
	{
		var newEnemy = new SimulatorEliteEnemy();

		newEnemy.Health = Health;
		newEnemy.Attack = Attack;
		newEnemy.ThreatValue = ThreatValue;
		newEnemy.BaseAttack = BaseAttack;
		newEnemy.BaseHealth = BaseHealth;

		newEnemy.CardName = CardName;
		newEnemy.Description = Description;
		newEnemy.Art = Art;
		newEnemy.EndTurnEffects = EndTurnEffects;

		return newEnemy;
	}
}
