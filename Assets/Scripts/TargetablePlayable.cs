﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TargetablePlayable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, ICardPlayable
{
    private Vector2 difference;
    public GameObject ArrowPrefab;
    private GameObject arrow;
    public float ToScreenRatio;
	public bool Disabled;

    public void OnBeginDrag(PointerEventData eventData)
    {
	    if (Disabled)
	    {
		    return;
	    }

        Debug.Log("DragBegin - " + name);
        ToScreenRatio = 1920f / Screen.width;

        arrow = Instantiate(ArrowPrefab, transform.position, Quaternion.identity);
        arrow.transform.SetParent(transform.Find("CardBody"));
        arrow.transform.SetAsFirstSibling();
    }

    public void OnDrag(PointerEventData eventData)
	{
		if (Disabled)
		{
			return;
		}

		var deltaVector = new Vector2(transform.position.x - eventData.position.x * ToScreenRatio,
            transform.position.y - eventData.position.y * ToScreenRatio);
    
        arrow.transform.eulerAngles = new Vector3(0, 0,
            CalculateAngle(-deltaVector.x,-deltaVector.y));

        arrow.transform.localPosition = new Vector3(0, 0, 0);
        arrow.transform.localScale = new Vector3(0.18f, deltaVector.magnitude / 965f);
    }

    private float CalculateAngle(float xDiff, float yDiff)
    {
        if (yDiff < 0)
        {
            return 180 - (float)System.Math.Atan(xDiff / yDiff) * 57.296f;
        }

        return (float)System.Math.Atan(-xDiff / yDiff) * 57.296f;
    }

    public void OnEndDrag(PointerEventData eventData)
	{
        Destroy(arrow);
		if (Disabled)
		{
			return;
		}

		GetComponent<TargetedSpellCardDisplay>().GameManager.HoveredObject = null;
		Debug.Log("DragEnd - " + name);

    }

	public void Disable()
	{
		Disabled = true;
	}

	public void Enable()
	{
		Disabled = false;
	}
}
