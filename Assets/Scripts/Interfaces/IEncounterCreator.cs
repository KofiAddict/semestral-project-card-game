﻿using System.Collections.Generic;

public interface IEncounterCreator
{
    EncounterData CreateEncounterData(ITree tree, List<Card> deck, int encounterNumber);
}
