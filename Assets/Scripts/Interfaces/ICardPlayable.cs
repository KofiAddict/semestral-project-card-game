﻿public interface ICardPlayable
{
	void Disable();
	void Enable();
}