﻿using System;
using System.Collections.Generic;
using System.Linq;

public class TurnSimulator
{
	private readonly TurnState startingState;
	private readonly List<TurnState> turnStates = new List<TurnState>();

	public TurnState BestTurnState { get; private set; }

	public TurnState MedianTurnState { get; private set; }

	public TurnSimulator(TurnState startingState)
	{
		this.startingState = startingState;
	}

	public void Simulate()
	{
		SimulateTurn(startingState);
		turnStates.Sort();
		BestTurnState = turnStates[0];
		MedianTurnState = turnStates[turnStates.Count / 2];
		turnStates.Clear();
	}

	private void SimulateTurn(TurnState state)
	{
		if (state.Hand.All(card => card.CostType == CostType.Energy && card.Cost > state.Energy || card.CostType == CostType.Mana && card.Cost > state.Mana)
		|| state.Health <= 0)
		{
			HandleResult(state);
			return;
		}
		if (state.Enemies.All(x => x.Health <= 0))
		{
			HandleResult(state);
			return;
		}

		foreach (var card in state.Hand)
		{
			if (card.CostType == CostType.Energy && card.Cost > state.Energy || card.CostType == CostType.Mana && card.Cost > state.Mana)
			{
				continue;
			}

			if (card.GetType() == typeof(SpellCard))
			{
				var nextState = state.Clone();
				var spellCard = (SpellCard) card;
				nextState.Hand.Remove(spellCard);
				
				if (spellCard.CostType == CostType.Energy)
				{
					nextState.Energy -= spellCard.Cost;
				}
				else
				{
					nextState.Mana -= spellCard.Cost;
				}

				switch (spellCard.BaseEffectType)
				{
					case EffectType.Heal:
						nextState.Health = nextState.Health + spellCard.EffectValue > nextState.MaxHealth
							? nextState.MaxHealth
							: nextState.Health + spellCard.EffectValue;
						break;
					case EffectType.Damage:
						foreach (var enemy in nextState.Enemies)
						{
							enemy.Health -= enemy.Health - spellCard.EffectValue < 0
								? enemy.Health
								: spellCard.EffectValue;
						}

						break;
					case EffectType.ReduceDamage:
						foreach (var enemy in nextState.Enemies)
						{
							enemy.Attack -= enemy.Attack - spellCard.EffectValue < 0 ? enemy.Attack : spellCard.EffectValue;
						}

						break;
					case EffectType.ManaFill:
						nextState.Mana = nextState.Mana + spellCard.EffectValue > nextState.MaxMana
							? nextState.MaxMana
							: nextState.Mana + spellCard.EffectValue;
						break;
					case EffectType.EnergyFill:
						nextState.Energy = nextState.Energy + spellCard.EffectValue > nextState.MaxEnergy
							? nextState.MaxEnergy
							: nextState.Energy + spellCard.EffectValue;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				SimulateTurn(nextState);
			}
			else
			{
				foreach (var enemy in state.Enemies)
				{
					var nextTarget = enemy.Clone();

					if (nextTarget.Health <= 0) continue;

					switch (card.BaseEffectType)
					{
						case EffectType.Heal:
							break;
						case EffectType.Damage:
							nextTarget.Health -= nextTarget.Health - card.EffectValue < 0
								? nextTarget.Health
								: card.EffectValue;
							break;
						case EffectType.ReduceDamage:
							nextTarget.Attack -= nextTarget.Attack - card.EffectValue < 0 ? nextTarget.Attack : card.EffectValue;
							break;
						case EffectType.ManaFill:
							break;
						case EffectType.EnergyFill:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					var enemies = state.Enemies.Except(new[] {enemy}).Select(x => x.Clone()).ToList();
					enemies.Add(nextTarget);
					var newHand = state.Hand.ToList();
					newHand.Remove(card);

					var nextState = state.Clone();
					nextState.Hand = newHand;
					nextState.Enemies = enemies;

					if (card.CostType == CostType.Energy)
					{
						nextState.Energy -= card.Cost;
					}
					else
					{
						nextState.Mana -= card.Cost;
					}

					SimulateTurn(nextState);
				}
			}
		}
	}

	private void HandleResult(TurnState state)
	{
		if (BestTurnState == null)
		{
			BestTurnState = state;
		}

		if (BestTurnState > state)
		{
			BestTurnState = state;
		}

		turnStates.Add(state);
	}

	public class TurnState : IComparable<TurnState>
	{
		public TurnState Parent;
		public List<SimulatorEnemy> Enemies;
		public List<Card> Hand;
		public int MaxHealth;
		public int MaxMana;
		public int MaxEnergy;
		public int Health;
		public int Mana;
		public int Energy;

		public TurnState Clone()
		{
			return new TurnState()
			{
				Parent = this,
				Enemies = Enemies.Select(x => x.Clone()).ToList(),
				Hand = Hand.ToList(),
				Health = Health,
				Energy = Energy,
				Mana = Mana,
				MaxEnergy = MaxEnergy,
				MaxMana = MaxMana,
				MaxHealth = MaxHealth
			};
		}

		public static bool operator <(TurnState one, TurnState other)
		{
			return one.CompareTo(other) < 0;
		}

		public static bool operator >(TurnState one, TurnState other)
		{
			return one.CompareTo(other) > 0;
		}

		public int CompareTo(TurnState state)
		{
			var remainingHealthThis = Health - Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);
			var remainingHealthThat = state.Health - state.Enemies.Where(x => x.Health > 0).Sum(x => x.Attack);

			if (remainingHealthThis <= 0 && remainingHealthThat > 0) return 1;
			if (remainingHealthThat <= 0 && remainingHealthThis > 0) return -1;
			if (remainingHealthThat <= 0 && remainingHealthThis <= 0) return 0;


			if (remainingHealthThat / (float)remainingHealthThis > 2 && remainingHealthThat - remainingHealthThis > 10) return 1;
			if (remainingHealthThis / (float)remainingHealthThat > 2 && remainingHealthThis - remainingHealthThat > 10) return -1;

			var elitesThis = Enemies.Where(x => x.Health > 0).Where(x => x.Rarity != EnemyRarity.Normal);
			var elitesThat = state.Enemies.Where(x => x.Health > 0).Where(x => x.Rarity != EnemyRarity.Normal);

			var hasDebuffingEliteThis = elitesThis.Any(x => x.EndTurnEffects.Contains(EndTurnEffect.DeBuffCards));
			var hasDebuffingEliteThat = elitesThat.Any(x => x.EndTurnEffects.Contains(EndTurnEffect.DeBuffCards));

			if (hasDebuffingEliteThat && !hasDebuffingEliteThis) return -1;
			if (hasDebuffingEliteThis && !hasDebuffingEliteThat) return 1;

			if (remainingHealthThat == remainingHealthThis)
			{
				return Enemies.Sum(x => x.Health < 0 ? 0 : x.Health) - state.Enemies.Sum(x => x.Health < 0 ? 0 : x.Health);
			}
			else
			{
				return remainingHealthThat - remainingHealthThis;
			}
		}
	}
}