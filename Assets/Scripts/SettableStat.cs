﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettableStat : MonoBehaviour
{
	public GameManager GameManager;

	public TextMeshProUGUI Level;
    public Image MeterImage;
	public Image PreviewMeterImage;

    public int MaxValue;
    public int CurrentValue;

    public void SetMeter(int newValue)
    {
        CurrentValue = newValue;
        Level.text = CurrentValue + "/" + MaxValue;
	    MeterImage.rectTransform.localScale = new Vector3(CurrentValue / (float) MaxValue, 1, 1);
	    PreviewMeterImage.rectTransform.localScale = new Vector3(CurrentValue / (float)MaxValue, 1, 1);
	}

    public bool TrySpend(int amount)
    {
        if (CurrentValue - amount >= 0)
        {
            SetMeter(CurrentValue - amount);
	        GameManager.CardCostAvailabilitySet();
			return true;
        }
        return false;
    }

    public void Fill(int amount)
    {
        if (CurrentValue + amount < MaxValue)
        {
            SetMeter(CurrentValue + amount); 
        }
        else
        {
            SetMeter(MaxValue);
        }

		GameManager.CardCostAvailabilitySet();
    }

	public void PreviewChange(int changeAmount)
	{
		changeAmount = -changeAmount;

		if (changeAmount < -CurrentValue)
		{
			changeAmount = -CurrentValue;
		}

		MeterImage.rectTransform.localScale = new Vector3((CurrentValue + changeAmount) / (float)MaxValue, 1, 1);
	}
}
