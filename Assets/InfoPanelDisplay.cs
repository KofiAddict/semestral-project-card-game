﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelDisplay : MonoBehaviour
{
	public Image Image;

	// 17 characters 8 lines
	public TextMeshProUGUI TopLeftText;
	public TextMeshProUGUI Title;
	public TextMeshProUGUI BottomText;

	public void Clear()
	{
		Image.sprite = Sprite.Create(Texture2D.blackTexture, Rect.MinMaxRect(0, 0, 4, 4), Vector2.zero, 1);
		TopLeftText.text = string.Empty;
		BottomText.text = string.Empty;
		Title.text = string.Empty;
	}

	public void Show(Enemy enemy)
	{
		Title.text = "Enemy Details";
		Image.sprite = enemy.Art;
		TopLeftText.text =
			"Name: " + enemy.CardName + "\n" +
			"Health: " + enemy.Health + "\n" +
			"Attack: " + enemy.Attack + "\n" +
			"Rarity: " + enemy.Rarity + "\n";

		var desc = string.Empty;
		for (int i = 0; i < enemy.EndTurnEffects.Count(); i++)
		{
			desc += "On the end of turn " + GetEndTurnEffectMessage(enemy.EndTurnEffects[i]) + " " + ((EliteEnemy)enemy).EndTurnEffectsValues[i] + "\n";
		}

		BottomText.text = desc;
	}

	public void Show(Card card)
	{
		Title.text = "Card Details";
		Image.sprite = card.Art;
		TopLeftText.text =
			"Name: " + card.CardName + "\n\n" +
			"Cost: " + card.Cost + "\n" +
			"Type: " + card.CostType + "\n";

		BottomText.text = string.Format(card.Description, card.EffectValue);
	}

	public void Show(TurnSimulator.TurnState state)
	{
		Title.text = "Turn Summary";
		Image.sprite = Sprite.Create(Texture2D.blackTexture, Rect.MinMaxRect(0, 0, 4, 4), Vector2.zero, 1);
		
		var damageTaken = state.Enemies.Sum(x => x.Health == 0 ? 0 : x.Attack);
		var topLeft =
			"Damage taken: " + damageTaken + "\n\n" +
			"Cards played: " + (5 - state.Hand.Count) + "\n" +
			"Health: " + state.Health + " -> " + (state.Health - damageTaken) + "\n" +
			"Enemies left: " + state.Enemies.Count(x => x.Health > 0) + "\n" +
			"Elites left: " + state.Enemies.Count(x => x.Health > 0 && x.Rarity != EnemyRarity.Normal)
			;

		TopLeftText.text = topLeft;

		var cardsPlayed = ReconstructCardsPlayed(state);

		var bottom = 
			"Cards played: \n"
			;

		foreach (var card in cardsPlayed)
		{
			bottom += card.CardName + "\n";
		}

		BottomText.text = bottom;
	}

	private List<Card> ReconstructCardsPlayed(TurnSimulator.TurnState state)
	{
		List<Card> cardsPlayed = new List<Card>();

		while (state.Parent != null)
		{
			var parent = state.Parent;

			var cards = parent.Hand.ToList();
			cards.RemoveAll(x => state.Hand.Contains(x));
			var card = cards[0];

			cardsPlayed.Add(card);

			state = state.Parent;
		}

		cardsPlayed.Reverse();
		return cardsPlayed;
	}

	public void Show(Node node)
	{

	}

	public void Show(SettableStat settableStat)
	{

	}

	private string GetEndTurnEffectMessage(EndTurnEffect enemyEndTurnEffect)
	{
		switch (enemyEndTurnEffect)
		{
			case EndTurnEffect.HealAll:
				return "heals all enemies for";
			case EndTurnEffect.HealSelf:
				return "heals itself for";
			case EndTurnEffect.ManaDrain:
				return "drains your mana for the next turn by";
			case EndTurnEffect.EnergyDrain:
				return "drains your energy for the next turn by";
			case EndTurnEffect.DeBuffCards:
				return "reduces effect of all your cards by";
			default:
				throw new ArgumentOutOfRangeException("enemyEndTurnEffect", enemyEndTurnEffect, null);
		}
	}

	public void Show(Card card, string infoMessage)
	{
		Show(card);
		BottomText.text = infoMessage;
	}

	public void Show(string infoMessage)
	{
		Clear();
		BottomText.text = infoMessage;
	}

	internal void Show(Enemy enemy, string infoMessage)
	{
		Show(enemy);
		BottomText.text = infoMessage;
	}
}
