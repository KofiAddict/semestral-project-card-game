﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SaveDisplay : MonoBehaviour
{
	public GameStarter GameStarter;
	public TextMeshProUGUI TitleTextMeshProUgui;
	public string SavePath;

	public void Setup(string saveName, string savePath)
	{
		TitleTextMeshProUgui.text = saveName;
		SavePath = savePath;
	}

	public void Load()
	{
		GameStarter.Load(SavePath);
	}
}
